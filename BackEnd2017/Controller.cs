﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json;

namespace BackEnd2017
{
    class Controller
    {
        Model M = new Model();

        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento
        public event viewEventHandler FlushText;  // questo genera l'evento

        public Controller()
        {
            M.FlushText += ControllerViewEventHandler; //Aggiungo un handler agli eventi del model 
        }

        private void ControllerViewEventHandler(object sender, string textToWrite)
        {
            FlushText(this, textToWrite);
        }

        public void DoSomething()  //Funzione chiamata dalla view, che risalirà fino al model 
        {
            M.DoSomething();
        }

        public void Connect(bool isSqlite, string idCliente)
        {

            string connString = "";
            if (isSqlite)
                connString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            else
                connString = ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString;

            Console.WriteLine(connString);

            M.Connect(connString, isSqlite, idCliente);
        }

        internal void ConnectToEF(bool isSQLite, string idCliente)
        {
            string sqLiteConnString = "";
            if (isSQLite)
            {
                FlushText(this, "Database not Supported");
            }
            else
            {
                List<ordini> ordini = M.ConnectToEF(idCliente);
                string json = JsonConvert.SerializeObject(ordini);
                FlushText(this, json);
            }
            
        }

        public void ConnectFactory(bool isSQLite, string idCliente)
        {
            string sqLiteConnString = "";
            string factory = "";
            if (isSQLite)
            {
                sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
                factory = "System.Data.SQLite";
            }
            else
            {
                sqLiteConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString;
                factory = "System.Data.SqlClient";
            }
            M.ConnectFactory(sqLiteConnString, factory, idCliente);
        }

        public void ArimaConnect(bool isSQLite, string idSerie, bool isForecast)
        {
            string sqLiteConnString = "";
            if (isSQLite)
            {
                sqLiteConnString = ConfigurationManager.ConnectionStrings["SQLiteConn"].ConnectionString;
            }
            else
            {
                sqLiteConnString = ConfigurationManager.ConnectionStrings["LocalDbConn"].ConnectionString;
            }
            M.ConnectArima(sqLiteConnString,isSQLite, idSerie, isForecast);
        }

    }
}
