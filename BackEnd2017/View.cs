﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackEnd2017
{
    public partial class View : Form
    {
        Controller C = new Controller();
        private bool isSqlite = false;
        public View()
        {
            InitializeComponent();
            radioButton1.Checked = false;

            var isSqlLt = ConfigurationManager.AppSettings["isSqlLite"];

            bool isSqlite = false;
            if (isSqlLt.Equals("1"))
            {
                radioButton1.Checked = true;
                isSqlite = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
            C.FlushText += ViewEventHandler; // associo il codice all'handler nella applogic
        }

        private void ViewEventHandler(object sender, string textToWrite)
        {
            txtConsole.AppendText(textToWrite + Environment.NewLine);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            C.DoSomething();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //bool isSqlite = radioButton1.Checked

            C.Connect(isSqlite, idClienteBox.Text);
        }

        private void ReadFactory_Click(object sender, EventArgs e)
        {
            C.ConnectFactory(radioButton1.Checked, idClienteBox.Text);
        }

        private void readEFButton_Click(object sender, EventArgs e)
        {
            C.ConnectToEF(radioButton1.Checked, idClienteBox.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            C.ArimaConnect(radioButton1.Checked, idClienteBox.Text, true);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            C.ArimaConnect(radioButton1.Checked, idClienteBox.Text, false);
        }
    }
}
