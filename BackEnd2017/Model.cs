﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using RDotNet;

namespace BackEnd2017
{
    class Model
    {
        public delegate void viewEventHandler(object sender, string textToWrite); // questo gestisce l'evento (mittente e testo generato per l'interfaccia)
        public event viewEventHandler FlushText;  // questo genera l'evento 
        public void DoSomething()
        {
            for (int i = 0; i < 10; i++)
                FlushText(this, $"i={i}");
        }

        public void Connect(string connString, bool isSqlite, string idCliente)

        {
             IDbConnection conn = null;

            try
            {
                if (isSqlite)
                    conn = new SQLiteConnection(connString);
                else
                    conn = new SqlConnection(connString);
                conn.Open();
                IDbCommand com = conn.CreateCommand();
                string queryText = "select idserie, periodo, val from histordini where idserie = @id";
                com.CommandText = queryText;

                IDbDataParameter param = com.CreateParameter();
                param.DbType = DbType.Int32;
                param.ParameterName = "@id";
                param.Value = Convert.ToInt32(idCliente);
                com.Parameters.Add(param);
                IDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    FlushText(this, reader["idserie"] + " " + reader["periodo"] + " " + reader["val"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                FlushText(this, e.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
           
        }

        public void ConnectArima(string connString, bool isSqlite, string idSerie, bool isForecast)

        {
            IDbConnection conn = null;
            var fileToSave = new StringBuilder();

            try
            {
                if (isSqlite)
                    conn = new SQLiteConnection(connString);
                else
                    conn = new SqlConnection(connString);
                conn.Open();
                IDbCommand com = conn.CreateCommand();
                string queryText = "select idserie, periodo, val from histordini where idserie = @id";
                com.CommandText = queryText;

                IDbDataParameter param = com.CreateParameter();
                param.DbType = DbType.Int32;
                param.ParameterName = "@id";
                param.Value = Convert.ToInt32(idSerie);
                com.Parameters.Add(param);
                fileToSave.AppendLine("ID,MONTH,SALE");
                IDataReader reader = com.ExecuteReader();
                while (reader.Read())
                {
                    //FlushText(this, reader["idserie"] + " " + reader["periodo"] + " " + reader["val"]);
                    fileToSave.AppendLine(reader["idserie"] + "," + reader["periodo"] + "," + reader["val"]);
                }

                File.WriteAllText("C:/Temp/series.csv", fileToSave.ToString());
                reader.Close();
                arimaForecasting(idSerie, isForecast);
            }
            catch (Exception e)
            {
                FlushText(this, e.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }

        }

        private void arimaForecasting(string idSerie, bool isForecast)
        {
            REngine engine;
            string path = System.Environment.GetEnvironmentVariable("Path");
            path = @"C:\Temp\R\R-3.4.2;" + @"C:\Temp\R\R-3.4.2\bin\i386;" + path;
            System.Environment.SetEnvironmentVariable("Path", path);
            path = System.Environment.GetEnvironmentVariable("Path");
            REngine.SetEnvironmentVariables();

            engine = REngine.GetInstance();
            engine.Initialize();
            engine.Evaluate("library(tseries)");
            engine.Evaluate("library(forecast)");
            engine.Evaluate("data <- read.csv(\"C:/Temp/series.csv\")");
            engine.Evaluate("myts <- ts(data[,3], start = c(" + idSerie + ", 0), end = c(" + idSerie + ", 34))");
            engine.Evaluate("arimafit<-auto.arima(myts, stepwise = FALSE, approximation = FALSE)");
            if (!isForecast)
            {
                engine.Evaluate("plot(arimafit$x, col=\"blue\")");
                engine.Evaluate("lines(fitted(arimafit), col=\"red\")");
            }
            else
            {
                engine.Evaluate("plot(forecast(arimafit, h=10))");
                engine.Evaluate("res = as.numeric(forecast(arimafit)$mean)");
                string[] predict = engine.Evaluate("res").AsCharacter().ToArray();
                for (int i = 0; i < predict.Length; i++)
                {
                    var temp = 35 + i;
                    FlushText(this, temp + "° numero predetto = " + predict[i]);
                }
            }
            engine.ClearGlobalEnvironment();
        }

        internal List<ordini> ConnectToEF(string idCliente)
        {
            dbTestEntities context = new dbTestEntities();
            List<ordini> ordini = new List<ordini>();
            foreach(ordini o in context.ordini)
            {


                    if (o.idcliente.Equals(Convert.ToInt32(idCliente)))
                {
                    ordini.Add(o);

                }
            }

            using (var context2 = new dbTestEntities())
            {
                var lstOrdini = context.ordini.SqlQuery("select idserie, periodo, val from histordini where idserie = @id", 
                    new SqlParameter("id", Convert.ToInt32(idCliente))).ToList();
                ordini = lstOrdini;
            }
            return ordini;

        }




        public void ConnectFactory(string sqLiteconnString, string factory, string idCliente)
        {
            DataSet ds = new DataSet();

            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(factory);
            using (DbConnection conn = dbFactory.CreateConnection())
            {
                try
                {
                    conn.ConnectionString = sqLiteconnString;

                    conn.Open();
                    IDbCommand com = conn.CreateCommand();
                    string queryText = "select idserie, periodo, val from histordini where idserie = @id";
                    com.CommandText = queryText;

                    IDbDataParameter param = com.CreateParameter();
                    param.DbType = DbType.Int32;
                    param.ParameterName = "@id";
                    param.Value = Convert.ToInt32(idCliente);
                    com.Parameters.Add(param);

                    IDataReader reader = com.ExecuteReader();
                    while (reader.Read())
                    {
                        FlushText(this, reader["idserie"] + " " + reader["periodo"] + " " + reader["val"]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    FlushText(this, ex.Message);
                }
                finally
                {
                    if (conn.State == ConnectionState.Open) conn.Close();
                }
            }
        }
    }
}
